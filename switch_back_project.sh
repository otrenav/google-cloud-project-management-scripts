#!/usr/bin/bash
#
# Switch back to initial gcloud project
#
# @author: Omar Trejo
# @date:   July, 2015
#

if [ $initial_project != $this_project ]; then
    gcloud config set project $initial_project
    echo "[+] Returned to initial gcloud project: $initial_project"
    echo "[+]"
fi
