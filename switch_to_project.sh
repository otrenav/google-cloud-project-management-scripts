#!/usr/bin/bash
#
# Switch to Google Cloud project
#
# @author: Omar Trejo
# @date:   July, 2015
#

# Get the current project in Google Cloud:
initial_project=`gcloud config list project | grep project | awk '{print $3;}'`
echo "[+] Current gcloud project: $initial_project"

if [ $initial_project != $this_project ]; then
    # Switch project:
    gcloud config set project $this_project
    echo "[+] Switched to gcloud project: $this_project"
    echo "[+]"
else
    echo "[+]"
fi
