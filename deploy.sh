#!/usr/bin/bash
#
# Stage 4:
# - Deploy code to Google App Engine
#
# @author: Omar Trejo
# @date:   July, 2015
#
# Requires:
# - Proyect
# - Path for repository

# deploy: realiza despliegue de código a Google App Engine (4).

# Note:
# - Enforce auto switch to new version

echo "deploy"

initial_directory=`pwd`

# TODO: Get this from configuration file
project_directory="/home/otrenav/Projects/datata/score-group/"

# Switch to directory to operate with git
cd $project_directory

git_branch=`git branch | awk '{print $2}'`

if [ ! -d "$project_directory.git" ]; then
    echo "[!] Error: directory does not contain git repository."
elif [ $git_branch != "master" ]; then
    echo "[!] Error: not on master branch in git respository."
else
    echo "deploy"
fi

# Switch back to initial directory
cd $initial_directory


#################################################

# Get parent process
parent_command="$(ps -o comm= $PPID)"

# Get the actual project in Google Cloud:
initial_project=`gcloud config list project | grep project | awk '{print $3;}'`
echo "[+] Proyecto activo: $initial_project"

# Switch to project:
this_project="score-group"
gcloud config set project $this_project
echo "[+] Cambio a proyecto: $this_project" 

# Deploy and make default version:
gcloud preview app deploy app.yaml --set-default
time=`date +%Y-%m-%d\ -\ %H:%Mh`
echo "[+] Se realizó el despliegue ($time)"

# Switch to initial project:
gcloud config set project $initial_project
echo "[+] Se regresó a proyecto: $initial_project"

echo "[+] Proceso finalizado exitosamente."
