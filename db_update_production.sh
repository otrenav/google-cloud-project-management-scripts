#!/usr/bin/bash
#
# Stage 5:
#- Update production database using develop database in Cloud SQL.
#
# @author: Omar Trejo
# @date:   July, 2015
#
# Requires:
# - Proyect
# - Production bucket
# - Cloud SQL instance
# - Cloud SQL database
#
# TODO:
# - Verify data
# - Logs
#

# db update production: actualiza BD remota de producción con migraciones (5).

echo "db update production"
