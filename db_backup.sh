#!/usr/bin/bash
#
# Stage 6:
# - Database backup
#
# @author: Omar Trejo
# @date:   July, 2015
#
# Requires:
# - Proyect
# - Production bucket
# - Google Drive path (local)
#
# TODO:
# - Verify data
# - Logs
#

# db backup: respaldo de BD de producción a Google Drive (6).

echo "[+]"
echo "[+] Database backup process:"

# Get backup from Google Storage:
time=`date +%Y-%m-%d\ -\ %H:%Mh`
gsutil cp $uri "$backup_directory"
echo "[+] Se archivo respaldo en Google Drive. ($time)"

# Create backup from Cloud SQL:
time=`date +%Y-%m-%d\ -\ %H:%Mh`

backup_file_name=`date +%Y-%m-%d-score-group-production-backup.sql`
uri="gs://$development_bucket/$backup_file_name"

gcloud sql cloud_sql_instances export $cloud_sql_instance $uri --database $cloud_sql_database_development
echo "[+] Backup created for: $cloud_sql_instance ($time)"
echo "[+] Backup has been placed in: $bakup_directory"
