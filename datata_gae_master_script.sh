#!/usr/bin/bash
#
# Datata's Google Cloud Project Management System
#
# Note: you should add this script to your available
#       terminal commands. You have two options:
#
#       1. Add this script to your PATH.
#       2. Make an alias to this script (I prefer this).
#
# @author: Omar Trejo
# @date:   July, 2015
#
# TODO:
# - Logs
#

echo "[+] ----------------------------------------"
echo "[+] ----     Datata - Google Cloud      ----"
echo "[+] ----------------------------------------"

if [ -z "$1" ]; then
    echo "[!]"
    echo "[!] Error: no command provided."
    echo "[!]"
    exit
fi

error=0

# Get this script's directory when it is executed:
script_directory=$(dirname "$(readlink -f "$(type -P $0 || echo $0)")")

#
# Configuration files
#

# Get the last argument
team_setup_file=${@: -1}

if [ ! -f $team_setup_file ]; then

    # If team setup file is not provided as the last
    # argument try to find it in the default location
    # given the current location in the terminal.

    if [ ! -f "./datata_setup/team/team_setup.sh" ]; then
       echo "[!]"
       echo "[!] Error: team setup file does not exist."
       echo "[!]"
       exit
    else
        # TODO: Make this default file a
        #       global configuraiton option.
        team_setup_file="./datata_setup/team/team_setup.sh"
    fi
fi

if [ -f $team_setup_file ]; then

    # Note: setup's correctness is
    # validated as needed within each sub-script.

    team_setup_file=`realpath $team_setup_file`
    source $team_setup_file

    # Note: currently, this script requries this file:
    #       - project_name/datata_setup/team/team_setup.sh
    #       - project_name/datata_setup/local/local_setup.sh
    #
    base_conf_file_directory=$(dirname  "${team_setup_file}")
    base_conf_file_directory=$(dirname  "${base_conf_file_directory}")
    base_conf_file_directory=$(dirname  "${base_conf_file_directory}")
    base_conf_file_directory=$(basename "${base_conf_file_directory}")

    local_setup_file=$(dirname "${team_setup_file}")
    local_setup_file=$(dirname "${local_setup_file}")
    local_setup_file="$local_setup_file/local/local_setup.sh"

    # The current directory should match the project name just
    # to be sure that you want to operate on this project's files.
    # If they are not equal, you may have an incorrect setup
    # in your setup file, and that can potentially be dangerous.
    #
    # Note: this safety check may be replaced in the future in
    #       favor of switching to the folder for the project
    #       specified and just output a warning.
    #
    if [ "$base_conf_file_directory" != "$this_project" ]; then
        echo "[!]"
        echo "[!] Error: Project name ($this_project) and containing"
        echo "[!]        directory ($base_conf_file_directory) do not match."
        echo "[!]"
        exit
    fi
    
    if [ ! -f $local_setup_file ]; then
        echo "[!]"
        echo "[!] Warning: local setup file does not exist."
        echo "[!]          MySQL and Google Drive backup will fail."
        echo "[!]"
    else
        source $local_setup_file
    fi
fi

#
# Commands
#
if [ $1 == "db" ]; then
    if [ -z $2 ]; then
        echo "[!]"
        echo "[!] Error: no argument provided for 'db' command."
        echo "[!]"
        error=1
    elif [ $2 == "update" ]; then
        if [ -z $3 ]; then
            echo "[!]"
            echo "[!] Error: no argument provided for 'db update' command."
            echo "[!]"
            error=2
        elif [ $3 == "develop" ]; then
            # Stage 2
            source $script_directory/switch_to_project.sh
            source $script_directory/db_update_develop.sh
        elif [ $3 == "production" ]; then
            # Stage 5
            source $script_directory/switch_to_project.sh
            source $script_directory/db_update_production.sh
        elif [ $3 == "local" ]; then
            # Stage 8
            source $script_directory/switch_to_project.sh
            source $script_directory/db_update_local.sh
        else
            echo "[!]"
            echo "[!] Error: 'db update' must be followed by 'develop',"
            echo "[!]        'production' or 'local'."
            echo "[!]"
            error=3
        fi
    elif [ $2 == "backup" ]; then
        # Stage 6
        source $script_directory/switch_to_project.sh
        source $script_directory/db_backup.sh
    else
        echo "[!]"
        echo "[!] Error: 'db' command must be followed by 'update' or 'backup'."
        echo "[!]"
        error=4
    fi
elif [ $1 == "deploy" ]; then
    if [ -z $2 ]; then
        # Stage 4
        source $script_directory/switch_to_project.sh
        source $script_directory/deploy.sh
    elif [ $2 == "full" ]; then
        # Stages 4, 5, 6, 8 and 9 (¡pum!)
        source $script_directory/switch_to_project.sh
        source $script_directory/deploy_full.sh
    elif [ $2 != "full" ]; then
        echo "[!]"
        echo "[!] Error: only argument allowed for 'deploy' command is 'full'."
        echo "[!]"
        error=5
    fi
elif [ $1 == "update" ]; then
    if [ -z $2 ]; then
        echo "[!]"
        echo "[!] Error: no argument given for 'update' command."
        echo "[!]"
        error=6
    elif [ $2 == "local" ]; then
        # Stages 8 and 9
        source $script_directory/switch_to_project.sh
        source $script_directory/update_local.sh
    elif [ $2 == "combo" ]; then
        # Stages 4 and 5
        source $script_directory/switch_to_project.sh
        source $script_directory/update_combo.sh
    fi
elif [ $1 == "git" ]; then
    if [ -z $2 ]; then
        echo "[!]"
        echo "[!] Error: no argument given for 'git' command."
        echo "[!]"
        error=7
    elif [ $2 == "update" ]; then
        # Stage 9
        source $script_directory/switch_to_project.sh
        source $script_directory/git_update.sh
    else
        echo "[!]"
        echo "[!] Error: unkwon command ('git' command must be followed by 'update')."
        echo "[!]"
        error=8
    fi
else
    echo "[!]"
    echo "[!] Error: unkwon command ($1)."
    echo "[!]"
    error=9
fi

#
# Cleanup
#
if [ $error -eq 0 ]; then
    source $script_directory/switch_back_project.sh
    echo "[+] The process ended succesfully."
    echo "[+]"
fi
