#!/usr/bin/bash
#
# Stage 8:
# - Update local MySQL database using production database in Cloud SQL
#
# @author: Omar Trejo
# @date:   July, 2015
#
# Requires:
# - Proyect
# - Production bucket
# - MySQL database
# - MySQL user
# - MySQL password (optional if user == root)
#
# TODO:
# - Verify data
# - Logs
#

# db update local: actualiza BD local con el último respaldo (8).

echo "db update local"
