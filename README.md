[![Visit our website](https://s31.postimg.org/9n8vhzwqj/datata.png)](http://links.datata.mx/datata-website)
[![Send us an email!](https://s31.postimg.org/xpo23w8yj/email.png)](mailto:contact@datata.mx)
[![Follow our blog!](https://s32.postimg.org/6l5f28pad/rss.png)](http://links.datata.mx/datata-blog)
[![See our work in GitHub!](https://s32.postimg.org/aan6j9vyd/github.png)](http://links.datata.mx/datata-github)
[![Like us on Facebook!](https://s32.postimg.org/b2lxxj2o5/facebook.png)](http://links.datata.mx/datata-fb)
[![Follow us on Twitter!](https://s31.postimg.org/8trwvjzyz/twitter.png)](http://links.datata.mx/datata-tw)
[![Follow us on LinkedIn!](https://s32.postimg.org/zeaaws26d/linkedin.png)](http://links.datata.mx/datata-linkedin)
[![Follow us in YouTube!](https://s31.postimg.org/f2navtpzf/youtube.png)](http://links.datata.mx/datata-youtube)

Contact us! We're always happy to solve complex problems!

---

# Google Cloud project management scripts

This scripts are used to manage Google Cloud projects with a focus on Django projects.

It focuses on managing the following areas:

- Repositories
- Databases
- Deployments

## Repositories management

> TODO: documentation

## Databases management

> TODO: documentation

## Deployments management

> TODO: documentation

Any feedback is welcome!

---

As always, have fun learning something new!
