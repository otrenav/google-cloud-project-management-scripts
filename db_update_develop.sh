#!/usr/bin/bash
#
# Stage 2:
# - Update develop database using production database in Cloud SQL.
#
# @author: Omar Trejo
# @date:   July, 2015
#
# Requires:
# - Proyect
# - Development bucket
# - Cloud SQL instance
# - Cloud SQL database
#
# TODO:
# - Verify data
# - Logs
#

# db update develop: actualiza BD remota de desarrollo con la de producción (2).

echo "db update develop"
