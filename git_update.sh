#!/usr/bin/bash
#
# Database backup
#
# @author: Omar Trejo
# @date:   July, 2015
#
# TODO:
# - Logs
#

# git update: actualiza las ramas master y develop del repositorio (9).

echo "[+] Updating git branches (git pull):"
printf "[+] - %s\n" "${branches_to_be_updated[@]}"

no_git_repo=`git st | grep "Not a git repository"`

if [ ! $no_git_repo ]; then
    for i in "${branches_to_be_updated[@]}"; do
        echo ""
        git checkout $i
        git pull origin $i
    done
    echo ""
    echo "[+]"
else
    echo "[!]"
    echo "[!] Error: No git repository found."
    echo "[!]"
    error=1
fi

